var typed = new Typed('#main-title', {
  strings: ['Collection of <span class="title-uncertain">uncertainties</span> / Sammlung von <span class="title-uncertain">Unsicherheiten</span>','Sammlung von <span class="title-uncertain">Unsicherheiten</span> / Collectie van <span class="title-uncertain">onzekerheden</span>', 'Collectie van <span class="title-uncertain">onzekerheden</span> / Collection of <span class="title-uncertain">uncertainties</span>', 'Collection des <span class="title-uncertain">incertitudes</span> / Collectie van <span class="title-uncertain">onzekerheden</span>'],
  typeSpeed: 200,
  backSpeed: 50,
  cursorChar: '',
  shuffle: true,
  smartBackspace: true,
  loop: true
});

// expand filter area

$(".passepartout").hover(function(){
    var itemNo = $(this).attr('class').split(' ')[1];
    var itemTitle = $(this).attr('title')
    $("span." + itemNo).css("color", "grey");
    // console.log("hovering over "+ itemNo + "attempting to hightlight span" + itemNo);
    $("#inventoryNb").html("current item: " + itemNo);
    $("#itemTitle").html("item title: " + itemTitle);
  }, function(){
    var itemNo = $(this).attr('class').split(' ')[1];
    $("span." + itemNo).css("color", "black");
});

$("span.description").hover(function(){
    // console.log("hovering over span.description")
    var itemNo = $(this).attr('class').split(' ')[0];
    // console.log(itemNo)
    $(".passepartout." + itemNo).css("outline", "3px solid yellow");
    $(".passepartout." + itemNo).css("filter", "none");
 }, function(){
    var itemNo = $(this).attr('class').split(' ')[0];
    $(".passepartout." + itemNo).css("outline", "none");
    $(".passepartout." + itemNo).css("filter", "greyscale(1) !important");
});

// Date & Time widgets

updateTime();
var update = setInterval(updateTime, 1000);
function updateTime(){
  var today = new Date();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  document.getElementById('time').innerHTML=time;
}
setDate();
function setDate(){
  var today = new Date();
  var date = today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate();
  document.getElementById('date').innerHTML=date;
}

// chartJS in footer

var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['possibl*', 'jamais', 'probabl*', 'presque', 'souvent', 'might', 'may'],
        datasets: [{
            label: 'results per word pattern',
            backgroundColor: 'rgb(0, 0, 0)',
            borderColor: 'rgba(255, 255, 255, 1)',
            data: [52, 22, 241, 30, 94, 4, 13]
        }]
    },
    options: {
    "responsive": true,
    "maintainAspectRatio": false,
    scales: {
        yAxes: [{ticks: {fontSize: 12, fontFamily: "'AntiqueNobleLight', sans-serif", fontColor: 'rgb(255,255,255)', fontStyle: '500'}}],
        xAxes: [{ticks: {fontSize: 12, fontFamily: "'AntiqueNobleLight', sans-serif", fontColor: 'rgb(255,255,255)', fontStyle: '500'}}]
    },
    legend: {
      labels: {
        "fontFamily": 'AntiqueNobleLight',
          }
        }
      }
});

// smooth scrolling on index

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

// filter by language

$(".toggle-FR").on("click", function(){
      $(this).toggleClass("striked");
      $(".language-Français").toggle();
});

$(".toggle-NL").on("click", function(){
      $(this).toggleClass("striked");
      $(".language-Nederlands").toggle();
});

$(".toggle-EN").on("click", function(){
      $(this).toggleClass("striked");
      $(".language-English").toggle();
});

$('.expand-filt').click(function(){
  $(".main-area").toggleClass("white-row white-row-collapsed");
  $(this).toggleClass("closed opened");
  $(".expand-filt-right").toggleClass("closed opened");
  $("#myChart").css("height", "100px");
});
$('.expand-filters-right').click(function(){
  $(".main-area").toggleClass("white-row white-row-collapsed");
  $(".expand-filt-right").toggleClass("closed opened");
  $(".expand-filt").toggleClass("closed opened");
});
