from flask import Flask, render_template, redirect, url_for
import csv
import os
import glob
import re

app = Flask(__name__)
thisdir = os.getcwd()
patternList = [r'(probabl\w*)', r'(possibl\w*)', r'(peut\-être)', r'(bijna)',\
r'(could)', r'(effectivement)' r'(éventuellement)', r'(jamais)', r'(may)', r'(might)', \
r'(misschien)', r'(mogelijk)', r'(must)', r'(ongeveer)', r'(parfois)', r'(perhaps)', r'(presque)', \
r'(probably)', r'(sometimes)', r'(souvent)', r'(waarschijnlijk)', r'(zeker)']
replace0 = r'<span class="uncertain_match">\1</span>'
replace1 = r'<span class="uncertain_match"> \1</span>'
uncertainlist = glob.glob('static/word_patterns/2019/*.csv')
uncertainlist2020 = glob.glob('static/word_patterns/2020/*.csv')

full_dict = {}
full_counter = 0

full_dict2020 = {}
full_counter2020 = 0

for file in uncertainlist:
    filename = file.split('/')[3]
    filename = filename.split('.')[0]
    file = open(file)
    file = csv.reader(file)
    currentlist = []
    for row in file:
        row[1] = row[1].replace(" ", "-")
        row[1] = row[1].replace(".", "-")
        for pattern in patternList:
            row[11] = re.sub(pattern, replace0, row[11])
        currentlist.append(row)
        full_counter += 1
    full_dict[filename] = currentlist

for file in uncertainlist2020:
    filename = file.split('/')[3]
    filename = filename.split('.')[0]
    file = open(file)
    file = csv.reader(file)
    currentlist2020 = []
    for row in file:
        row[1] = row[1].replace(" ", "-")
        row[1] = row[1].replace(".", "-")
        for pattern in patternList:
            row[11] = re.sub(pattern, replace0, row[11])
        currentlist2020.append(row)
        full_counter2020 += 1
    full_dict2020[filename] = currentlist2020

# print(patternList)

amounts = []
for word, results in full_dict.items():
    word_amount = word, len(results)
    amounts.append(word_amount)

amounts2020 = []
for word2020, results2020 in full_dict2020.items():
    word_amount2020 = word2020, len(results2020)
    amounts2020.append(word_amount2020)

uncertainlist = glob.glob('static/word_patterns/2019/*.csv')
uncertainlist = [i.split('/')[3] for i in uncertainlist]
uncertainlist = [i.split('.')[0] for i in uncertainlist]

uncertainlist2020 = glob.glob('static/word_patterns/2020/*.csv')
uncertainlist2020 = [i.split('/')[3] for i in uncertainlist2020]
uncertainlist2020 = [i.split('.')[0] for i in uncertainlist2020]


@app.route("/")
@app.route("/2020")
def twenty():
    return render_template("index2020.html", full_dict2020=full_dict2020, uncertainlist2020=uncertainlist2020, full_counter2020=full_counter2020, amounts2020=amounts2020)

@app.route("/2019")
def nineteen():
    data_file = open('scrape_probabl.csv')
    csv_file = csv.reader(data_file)
    file = csv.reader(data_file)
    for row in file:
        print(row[11])

    return render_template("index.html", full_dict=full_dict, uncertainlist=uncertainlist, csv_file=csv_file, file=file, full_counter=full_counter, full_counter2020=full_counter2020, amounts=amounts)

@app.route('/collections')
def collections():
    return render_template("collections.html")

@app.route('/collection2019')
def collection2019():
    return render_template("collection2019.html", full_counter=full_counter, uncertainlist=uncertainlist, file=file, amounts=amounts)

@app.route('/collection2019/')
def redirectCollection2019():
    return redirect(url_for('collection2019'))

@app.route('/collection2020')
def collection2020():
    return render_template("collection2020.html", full_counter2020=full_counter2020, uncertainlist2020=uncertainlist2020, amounts2020=amounts2020)

@app.route('/collection2020/')
def redirectCollection2020():
    return redirect(url_for('collection2020'))

@app.route('/collection2019/<uncertain>')
def word_pattern(uncertain):
    uncertainlist = glob.glob('static/word_patterns/2019/*.csv')
    uncertainlist = [i.split('/')[3] for i in uncertainlist]
    uncertainlist = [i.split('.')[0] for i in uncertainlist]
    if uncertain in uncertainlist:
        print('this is a file')
        filename = 'static/word_patterns/2019/' + uncertain + '.csv'
        datafile = open(filename)
        datafile = csv.reader(datafile)
        print(filename)
    else:
        return render_template('error.html'), 404

    return render_template("uncertain2019.html", uncertain=uncertain, datafile=datafile)

@app.route('/collection2020/<uncertain>')
def word_pattern2020(uncertain):
    uncertainlist = glob.glob('static/word_patterns/2019/*.csv')
    uncertainlist = [i.split('/')[3] for i in uncertainlist]
    uncertainlist = [i.split('.')[0] for i in uncertainlist]
    if uncertain in uncertainlist2020:
        print('this is a file')
        filename = 'static/word_patterns/2020/' + uncertain + '.csv'
        datafile = open(filename)
        datafile = csv.reader(datafile)
        print(filename)
    else:
        return render_template('error.html'), 404

    return render_template("uncertain2019.html", uncertain=uncertain, datafile=datafile)


@app.route("/about")
def about():
    return render_template("about.html", full_counter2020=full_counter2020, uncertainlist=uncertainlist, file=file, amounts=amounts)

@app.route('/about/')
def redirectAbou():
    return redirect(url_for('about'))

@app.route("/news")
def news():
    return render_template("news.html", full_dict=full_dict, full_counter2020=full_counter2020, amounts=amounts)

@app.route('/news/')
def redirectNews():
    return redirect(url_for('news'))

@app.errorhandler(404)
def not_found(error):
    return render_template('error.html'), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5001')
